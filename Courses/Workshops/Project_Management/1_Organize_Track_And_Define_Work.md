## Create Group, Project, Labels, Boards, Project Milestone, and Issues

### Theme

Organizing and planning work

### Key Tasks to Complete

# Step 1: Create a new Subgroup

1. Before we move over our template project, go ahead and click **Create new subgroup** in the group redeemed for us
  
2. Under **Subgroup name**, provide a group name (**Tanuki-Racing-Group**)

3. Leave all other settings as is

4. Click **Create subgroup**
 
# Step 2: Import Our Project

1. Click **Create new project**
  
2. Click **Import project**
  
3. Click **Repository by URL**
  
4. In the _Git repository URL_ section add the following URL: https://gitlab.com/gitlab-learn-labs/sample-projects/tanuki-racing.git
  
5. Leave all other settings as is.
  
6. Click **Create project**
  
7. You may see a note regarding an SSH key. It is okay to ignore this message during the workshop.

> [Docs for setting up your org](https://docs.gitlab.com/ee/topics/set_up_organization.html)

> **At this point you should be in your project, not the subgroup**

# Step 3: Create at least 4 labels

1. Use the left hand navigation menu to click through **Manage** > **Labels**
  
2. Click **New label**
  
3. Enter **P1** in the ***Title*** field and select a color
  
4. Click **Create label**
  
5. Click **New label** and create another label for **P2**
  
6. Click **New label** and create another label for **Workflow::Planning**
  
7. Click **New label** and create another label for **Workflow::Review**

# Step 4: Create New Board

1. Use the left hand navigation menu to click through **Plan -> Issue boards**
  
2. Click **Create new board** within the dropdown that says **Development**.

3. Enter a title (**Team-Scoped-Board**)
  
4. Leave **Show the Open list** and **Show the Closed list** checkboxes selected
  
5. Click **Create board**

> You may need to swap to your new board if it dosent change automatically
  
6. Click **Create list**. Leave the scope of the list set to **Label**. Select **Workflow::Planning**. Click **Add to board**.
  
7. Click **Create list**. Leave the scope of the list set to **Label**. Select **Workflow::Review**. Click **Add to board**.  As we add issues in the future if they contain the scoped labels they will automatically appear in the appropriate column based on their labels.
  
8. On the **Workflow::Review** column, click the gear tool in the upper right-hand corner of the column to open the column settings.  Next to **Work in progress limit**, click **Edit**, then enter a value of 1.  The value will auto-save when you click out of the box.  Click the X to close the List settings.

# Step 5: Create Project Milestone

1. Use the left hand navigation menu to click through **Plan**>**Milestones**
  
2. Click **New milestone**
  
3. Enter a title (**Tanuki-Racing-Migration-Milestone**)
  
4. Set a start date as **today**
  
5. Set a due date as **one week from today**
  
6. Click **Create milestone**

# Step 6: Create Issues

1. Create an Issue
    * Navigate to **Plan>Issues** using the left hand navigation menu
    * Click **New issue**
    * Give a title **Update-Pipeline**
    * In the **Description** area, enter the text
    ```
    We will move to GitLab CI and take advantage of the Auto DevOps pipeline template to create a pipeline that contains the testing and security scans we are looking to implement.

    __Acceptance criteria__
     - [ ] Auto DevOps has been enabled for the project and the auto devops pipeline completes successfully
     - [ ] the unit test and security scan job outputs have been reviewed
     - [ ] SAST scanning has been [tuned](https://docs.gitlab.com/ee/user/application_security/sast/customize_rulesets.html) for our specific use case with the specific ruleset provided by our Security Team
    ```
    * Click **Preview** to see the markdown formatting view and validate that the formatting looks good
    * Click **Assign to me** to assign the issue to yourself. Even if your name doesn't appear in the dropdown you can still write in your GitLab id
    * Select the milestone (**Tanuki-Racing-Migration-Milestone**)
    * Assign the labels **P2** and **Workflow::Review**
    * Give a weight (i.e. **2**)
    * Select a due date within the next week
    * Leave all other settings as is
    * Click **Create issue**

2. Create an Issue using Quick Actions
    * Navigate to **Plan>Issues** using the left hand navigation menu
    * Click **New issue**
    * Give a title **Configure DAST Scanner**
    * Under the ***Description Dropdown*** select workshop_template.
    * Your ***Write*** section should now be populated with some quick actions and a template. 
    * Go ahead and complete the template
    * Click **Create issue**

> [Docs for setting up your org](https://docs.gitlab.com/ee/user/project/quick_actions.html)

# Step 7: Move Issues in Boards

1. Navigate to **Plan**>**Issue Boards**
  
2. If not already selected, select the **Team-Scoped-Board** board from the board selector dropdown
  
3. Note the issue in the **Open** list & the one issue in the _Workflow::Review_ list
  
4. Click and drag the **Configure DAST Scanner** issue into the **Workflow::Planning** list
  
5. Click on that issue and note the labels now include **Workflow::Planning**
  
6. Click and drag the same issue into **Workflow::Review**
  
7. Note the issue labels now include **Workflow::Review** and *not* **Workflow::Planning**; additionally, notice that the **Workflow::Review** column turns red as the work in progress limit has been exceeded.
  
8. Click and drag the same issue into Closed. This closes the issue.  It also turns the **Workflow::Review** column white again as there is now only 1 issue  - within the work in progress limit
    
9. Before moving on, re-open the issue by dragging the closed issue back to ***Workflow::Planning***

# Step 8: Review project milestone charts
  
1. Navigate to **Plan**>**Milestones**
  
2. Click **Tanuki-Racing-Migration-Milestone**
  
3. Notice the Burndown and Burnup charts
  
4. Notice the Unstarted Issues, Ongoing Issues, and Completed Issues tracked part of the milestone


